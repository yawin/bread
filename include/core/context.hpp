#pragma once

#ifdef BRD_BUILD_DLL
  #include "core/components.hpp"
  #include "core/entities.hpp"
  #include "core/slotmap.hpp"
  #include "core/type_traits.hpp"
  #include "core/utils.hpp"
  #include "typedeclarations.hpp"
#else
  #include <BreadEngine/core/components.hpp>
  #include <BreadEngine/core/entities.hpp>
  #include <BreadEngine/core/slotmap.hpp>
  #include <BreadEngine/core/type_traits.hpp>
  #include <BreadEngine/core/utils.hpp>
  #include <BreadEngine/typedeclarations.hpp>
#endif


#include <iostream>

namespace brd
{
  namespace core
  {
    class Context
    {
      public:
        explicit Context() = default;

        //template<typename... CMPS>
        inline Entity& CreateEntity()
        {
          const auto& e = entities.push(Entity{});
          auto& entity = GetEntity(e);
          entity.id = e;

          //(... AddComponent<CMPS>(e) );
          return entity;
        }

        inline void DeleteEntity(const brdID& entityID)
        {
          RemoveAllComponents(entityID);
          entities.erase(entityID);
        }

        inline Entity& GetEntity(const brdID& entityID)
        {
          auto& e = entities[entityID];
          return e;
        }

        template<typename T, typename... InitTypes>
        requires(!IsSingleComponent<T>) // Se ejecuta si T no hereda de SingleComponent
        T& AddComponent(Entity& entity, InitTypes&&... initVals)
        {
          auto rCMP = components.CreateComponent<T>(entity.id, initVals...);
          entity.addComponent<T>(rCMP);
          return components.GetComponents<T>()[rCMP];
        }

        template<typename T, typename... InitTypes>
        requires(IsSingleComponent<T>) // Se ejecuta si T hereda de SingleComponent
        T& AddComponent(Entity& entity, InitTypes&&... initVals)
        {
          if(!entity.hasComponent<T>())
          {
            auto rCMP = components.CreateComponent<T>(entity.id, initVals...);
            entity.addComponent<T>(rCMP);
            return components.GetComponents<T>()[rCMP];
          }
          else
          {
            return components.GetComponents<T>()[entity.getComponent<T>().value()];
          }
        }

        template<typename T>
        slotmap<T>& GetComponents()
        {
          return components.GetComponents<T>();
        }

        template<typename T>
        T& GetComponent(brdID cmpID)
        {
          return components.GetComponent<T>(cmpID);
        }

        template<typename T>
        void RemoveComponents(Entity& entity)
        {
          for(auto& c : entity.getComponents<T>())
          {
            components.RemoveComponent<T>(c);
          }

          entity.removeComponents<T>();
        }

        void RemoveAllComponents(const brdID& entityID)
        {
          RemoveAllComponents(entities[entityID]);
        }

        void RemoveAllComponents(Entity& entity)
        {
          removeAllComponents<brd::ComponentManagerType>(entity);
        }

      private:
        slotmap<Entity> entities;
        ComponentManager<brd::ComponentManagerType> components;


        template<typename CL>
        void removeAllComponents(Entity& entity)
        {
          if constexpr (CL::size() > 0)
          {
            RemoveComponents<type_traits::head<CL>>(entity);

            if constexpr (CL::size() > 1)
            {
              removeAllComponents<type_traits::pop<CL>>(entity);
            }
          }
        }
    };
  };
};
