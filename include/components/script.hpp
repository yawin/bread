#pragma once


#ifdef BRD_BUILD_DLL
  #include "core/components.hpp"
  #include "core/context.hpp"
  #include "core/internal_api.hpp"
  #include "core/utils.hpp"
#else
  #include <BreadEngine/core/components.hpp>
  #include <BreadEngine/core/context.hpp>
  #include <BreadEngine/core/internal_api.hpp>
  #include <BreadEngine/core/utils.hpp>
#endif

#include <format>
#include <map>

namespace brd
{
  class Script : public core::Component
  {
    public:
      explicit Script(core::brdID entityID);
      virtual ~Script();

      virtual void Start(core::Context& ctxt) {}
      virtual void Update(core::Context& ctxt) {}

      template<typename R, typename... CallTypes>
      void AddCallback(std::string_view key, std::function<R(CallTypes...)> callback)
      {
        std::string key_ = std::format("{}:{}", key, eID);
        brd::core::InternalApi::AddCallback<void, float&&>(key_, std::move(callback));

        auto it = callbackids.find(key_);
        if(it == callbackids.end())
        {
          callbackids[key_] = [&](std::string_view key){ brd::core::InternalApi::DelCallback<R, CallTypes...>(key); };
        }
      };

    private:
      std::map<std::string, std::function<void(std::string_view)>> callbackids {};
  };
};
